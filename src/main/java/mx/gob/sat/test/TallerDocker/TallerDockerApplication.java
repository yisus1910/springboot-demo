package mx.gob.sat.test.TallerDocker;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TallerDockerApplication {
    
    @Autowired
	private Environment env;
    public static void main(String[] args) {
		SpringApplication.run(TallerDockerApplication.class, args);
                System.out.println("Se inicia el servicio Taller Docker v.1.0");
	}
        
        @GetMapping("test_avl")
        public String test_avl(){
            String ambiente = env.getProperty("AMBIENTE");
            System.out.println("Info: Se consume el servicio, del ambiente:"+ambiente);
          
            String mensaje = "Bienvenido al Demo Taller Docker, si puedes visualizar éste mensaje, el servicio esta arriba!-v1.0.0- Servicio del ambiente "+ambiente;
            
            return mensaje;  
        }
}
